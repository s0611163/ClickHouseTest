CREATE TABLE mytest.people_face_replica (
`captured_time` DateTime,
 `face_id` String,
 `camera_id` String,
 `data_source3` String,
 `panoramic_image_url` String,
 `camera_fun_type` String,
 `portrait_image_url` String,
 `extra_info` String,
 `id_card` String,
 `name` String,
 `event` String
) ENGINE = ReplicatedMergeTree('/clickhouse/tables/{layer}-{shard}/people_face_replica', '{replica}') PARTITION BY toYYYYMMDD(captured_time) ORDER BY (captured_time, face_id, camera_id, data_source3) TTL captured_time + toIntervalMonth(12) SETTINGS storage_policy = 'data_jbod', index_granularity = 8192
